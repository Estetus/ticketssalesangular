import {Component, OnDestroy, OnInit} from '@angular/core';
import {ObservableExampleService} from "../../services/observable/observable.service";
import {Subject, Subscription, take} from "rxjs";
import {SettingsService} from "../../services/settings/settings.service";
import {MessageService} from "primeng/api";
import {IUser} from "../../models/users";



@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  login: string;
  settingsData: Subscription;
  settingsDataSubject: Subscription
  psw: string;
  pswRepeat: string;
  newPsw: string

  constructor(private  observable: ObservableExampleService,
              private settingsService: SettingsService,
              private  messageService: MessageService) {
    observable.initObservable()
  }

  ngOnInit(): void {

    this.settingsData = this.settingsService.loadUserSetings().subscribe((data) => {
      console.log('settings data', data)
    });

    this.settingsDataSubject = this.settingsService.getSettingsSubjectObservable().pipe(take(1)) .subscribe(
      (data)=> {
        console.log('settings data from subject', data)
      }
    )


  }

  // changePassword(ev: Event): void | boolean {
  //   if(this.newPsw !== this.pswRepeat) {
  //     this.messageService.add({severity: 'error', summary: 'Пароли не совпадают!'});
  //     return false;
  //   }

  //   const userObj: IUser = {
  //     psw: this.psw,
  //     login: this.login,
  //   }

  //   if(this.authService.checkUser(authUser)) {
  //     this.authService.setUser(authUser);
  //     this.userService.setUser(authUser)

  //     this.userService.setToken('user-token')

  //     this.router.navigate(['tickets/tickets-list'])
  //     this.messageService.add({severity: 'success', summary: 'Авторизация прошла успешно'});
  //   } else  {
  //     this.messageService.add({severity: 'error', summary: 'Пользователь ввел неверные данные'});
  //   }

  // }
  ngOnDestroy(): void {
    this.settingsData.unsubscribe()
  }



}
